package stepDefs;

import com.jayway.jsonpath.JsonPath;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import pojo.pets.Category;
import pojo.pets.CreatePet;
import pojo.pets.Tags;
import pojo.pets.UpdatePet;
import utils.CommonUtils;

import java.util.Collections;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static utils.Hooks.*;

public class PetsCrud {

    int actualPetId;

    @Given("Create a pet with the following data and send a POST request")
    public void createAPetWithTheFollowingDataAndSendAPOSTRequest(Map<String, String> petPost) {

        category = Category.builder()
                .id(Integer.parseInt(petPost.get("categoryId")))
                .name(petPost.get("categoryName")).build();

        tags = Tags.builder()
                .id(Integer.parseInt(petPost.get("tagId")))
                .name(petPost.get("tagName")).build();

        createPet = CreatePet.builder()
                .id(CommonUtils.genRandomNumber(2))
                .name(petPost.get("petName"))
                .category(category)
                .photoUrls(Collections.singletonList(petPost.get("photoURL")))
                .tags(Collections.singletonList(tags))
                .status(petPost.get("status")).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(createPet)
                .post(petStoreUrl + "/v2/pet")
                .then().log().all().assertThat().statusCode(200)
                .extract().response();

        actualPetId = JsonPath.read(response.asString(), "id");

        logger.debug("The value is not matching with expected one");
        logger.error("Not abele match " + createPet.getName() + " with " + JsonPath.read(response.asString(), "name"));

        assertThat(
                "I am expecting pet name is: " + createPet.getName(), // message of the reason
                JsonPath.read(response.asString(), "name"), // actual value from the response
                is(createPet.getName()) // expected value from the request
        );

        assertThat(
                "I am expecting tag name is: " + tags.getName(),
                JsonPath.read(response.asString(), "tags[0].name"),
                is(tags.getName()));

    }

    @And("I send a GET request to {string}")
    public void iSendAGETRequestToWithPetId(String getUrl) {

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .get(petStoreUrl + getUrl + actualPetId)
                .then().log().all().assertThat().statusCode(200)
                .extract().response();
    }

    @And("I send a PUT request with the following data:")
    public void iSendAPUTRequestWithTheFollowingData(Map<String, String> petPut) {

        category = Category.builder()
                .id(Integer.parseInt(petPut.get("categoryId")))
                .name(petPut.get("categoryName")).build();

        tags = Tags.builder()
                .id(Integer.parseInt(petPut.get("tagId")))
                .name(petPut.get("tagName")).build();

        updatePet = UpdatePet.builder()
                .id(actualPetId)
                .name(petPut.get("petName"))
                .category(category)
                .photoUrls(Collections.singletonList(petPut.get("photoURL")))
                .tags(Collections.singletonList(tags))
                .status(petPut.get("status")).build();

        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .body(createPet)
                .put(petStoreUrl + "/v2/pet")
                .then().log().all().assertThat().statusCode(200)
                .extract().response();

        // logger should be above the assertion so, we can log the issue
        logger.debug("The value is not matching with expected one");
        logger.error("Not abele match " + createPet.getName() + " with " + JsonPath.read(response.asString(), "name"));

        assertThat(
                "I am expecting pet name is: " + createPet.getName(),
                JsonPath.read(response.asString(), "name"),
                is(createPet.getName())
        );

        assertThat(
                "I am expecting tag name is: " + tags.getName(),
                JsonPath.read(response.asString(), "tags[0].name"),
                is(tags.getName()));

    }

    @When("I send a DELETE request to {string}")
    public void iSendADELETERequestToWithPetId(String deleteUrl) {
        response = RestAssured.given().log().all()
                .contentType(ContentType.JSON)
                .delete(petStoreUrl + deleteUrl + actualPetId)
                .then().log().all().assertThat().statusCode(200)
                .extract().response();
    }

    @And("Validate that status code is {int} in common step def")
    public void validateThatStatusCodeIsInCommonStepDef(int statusCode) {
        int actualStatusCode = response.getStatusCode();

        assertThat(
                "I am expecting status code: " + statusCode,
                actualStatusCode,
                is(statusCode)
        );
    }
}
