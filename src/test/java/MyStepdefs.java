import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class MyStepdefs {
    @Given("user creates a pet with the following data and sends a POST request")
    public void userCreatesAPetWithTheFollowingDataAndSendsAPOSTRequest() {
        
    }

    @Then("validate the status code is 200 in common stepDef")
    public void validateTheStatusCodeIsInCommonStepDef(int arg0) {
        
    }

    @And("user updates pet with the following the information and sends a PUT request")
    public void userUpdatesPetWithTheFollowingTheInformationAndSendsAPUTRequest() {
        
    }

    @And("user gets the body by sending a GET request to {string} with the id {int}")
    public void userGetsTheBodyBySendingAGETRequestToWithTheId(String arg0, int arg1) {
        
    }

    @And("user send a GET request to {string}")
    public void userSendAGETRequestTo(String arg0) {
        
    }

    @And("user send a DELETE request to {string}")
    public void userSendADELETERequestTo(String arg0) {
        
    }

    @And("user sends a POST request to {string}")
    public void userSendsAPOSTRequestTo(String arg0) {
    }

    @Given("Create a pet with the following data and send a POST request")
    public void createAPetWithTheFollowingDataAndSendAPOSTRequest() {
    }
}
